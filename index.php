<?php

define( 'ABSPATH', dirname( __FILE__ ) . '/' );

require ABSPATH . '/vendor/autoload.php';

require ABSPATH . '/routes/routes.php';


function home()
{
    include('view/home.php');
}

function about()
{
    include('view/about.php');
}

