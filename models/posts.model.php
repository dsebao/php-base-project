<?php

function getPosts(){
    $client = new GuzzleHttp\Client();
    $res = $client->request('GET', 'https://jsonplaceholder.typicode.com/posts');

    $return = array(
        'status' => $res->getStatusCode(),
        'body' => json_decode($res->getBody(),true)
    );
    return $return;
}
